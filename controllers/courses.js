//[SECTION] Dependencies and Modules
  const Course = require('../models/Course');

//[SECTION] Functionality [CREATE]
   module.exports.createCourse = (info) => {
     let cName = info.name;
     let cDesc = info.description;
     let cCost = info.price;
     let newCourse = new Course({
      name: cName,
      description: cDesc,
      price: cCost
     }) 
     return newCourse.save().then((savedCourse, error) => {
      if (error) {
        return 'Failed to Save New Document';
      } else {
        return savedCourse; 
      }
     });
   };

//[SECTION] Functionality [RETRIEVE]

  //find all documents in the Course collection
  //We will use the find() method of our Course model
  //Because the Course model is connected to our courses collection
  //Course.find({}) = db.courses.find({})
  //empty {} will return all documents
   module.exports.getAllCourse = () => {
     return Course.find({}).then(outcomeNiFind => {
        return outcomeNiFind;
     });
   };

   module.exports.getCourse = (id) => {
      return Course.findById(id).then(resultOfQuery => {
        return resultOfQuery; 
      });
   };

   module.exports.getAllActiveCourse = () => {
      return Course.find({isActive: true}).then(resultOftheQuery => {
          return resultOftheQuery;
      });
   };

//[SECTION] Functionality [UPDATE]
   module.exports.updateCourse = (id, details) => {
      let cName = details.name; 
      let cDesc = details.description;
      let cCost = details.price;   
      let updatedCourse = {
        name: cName,
        description: cDesc,
        price: cCost
      }
      return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
          if (err) {
            return 'Failed to update Course'; 
          } else {
            return 'Successfully Updated Course'; 
          }
      })
   }

   module.exports.deactivateCourse = (id) => {
     let updates = {
       isActive: false
     } 
     return Course.findByIdAndUpdate(id, updates).then((archived, error) => {
        if (archived) {
          return `The Course ${id} has been deactivated`;
        } else {
          return 'Failed to archive course'; 
        };
     });
   };
  

//[SECTION] Functionality [DELETE]
  module.exports.deleteCourse = (id) => {
    return Course.findByIdAndRemove(id).then((removedCourse, err) => {
        if (err) {
          return 'No Course Was Removed'; 
        } else { 
          return 'Course Succesfully Deleted'; 
        };
    });
  };

  //Produce a demo video displaying your milestone for update course already deployed and built in heroku. sent in Hangouts

  // *once done you are free to go, and enjoy the rest of the afternoon. Thank you*
  //5:00 pm
