//[SECTION] Dependencies and Modules
  const exp = require("express"); 
  const controller = require('./../controllers/users.js');

//We will import auth module so we can use our verify and verifyAdmin method as middleware for our routes.
  const auth = require("../auth") 

//destructure verify and verifyAdmin from auth. Auth, is an imported module, so therefore it is an object in JS.
  const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 


//[SECTION] Routes-[POST]
  route.post('/register', (req, res) => {
    let userDetails = req.body; 
    controller.registerUser(userDetails).then(outcome => {
       res.send(outcome);
    });
  });
  


// LOGIN
  route.post("/login", controller.loginUser);



// GET USER DETAILS
  // note: routes that have verify as a middleware would require us to pass a token from postman
  route.get("/getUserDetails", verify, controller.getUserDetails)




//ENROLL OUR REGISTERED USER
  route.post('/enroll', verify, controller.enroll)




//GET LOGGED USER'S ENROLLMENTS
route.get('/getEnrollments', verify, controller.getEnrollments)





//[SECTION] Expose Route System
  module.exports = route; 
